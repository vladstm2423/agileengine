# Photos Uploader
 # run server :
    virtualenv venv -p python3
    source venv/bin/activate
    pip install -r requrements.txt
    ./manage.py runserver 0.0.0.0:8000
# To fetch all photos
    ./manage.py fetch_photos
# Search API URL after running server: http://localhost:8000/photos/search/q=canon
For periodical running we can use one of the following:
- make a cron job, which will run "./manage.py fetch_photos"
- install celery, and make a periodical task 