from django.urls import path

from .views import PhotosSearchAPIView


urlpatterns = [
    path('search/', PhotosSearchAPIView.as_view(), name='search-photos')
]
