from django.db.models import Q

from rest_framework.generics import ListAPIView

from .serializers import PhotoSerializer
from .models import Photo


class PhotosSearchAPIView(ListAPIView):
    serializer_class = PhotoSerializer
    queryset = Photo.objects.all()

    def filter_queryset(self, queryset):
        q = self.request.GET.get('q')
        if q:
            queryset = queryset.filter(
                Q(author__icontains=q)
                | Q(camera__icontains=q)
                | Q(tags__icontains=q)
            )
        return queryset
