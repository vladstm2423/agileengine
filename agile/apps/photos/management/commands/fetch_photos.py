from django.core.management.base import BaseCommand, CommandError

from photos.utils import PhotosDownloader
from photos.models import Photo


class Command(BaseCommand):
    help = 'Fetch photos'

    def handle(self, *args, **options):
        photos_downloader = PhotosDownloader()
        photos_downloader.get_photos_list()
        photo_ids = Photo.objects.values_list('id', flat=True)
        for photo_id in photo_ids:
            photos_downloader.get_photo(photo_id)