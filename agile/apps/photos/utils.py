import requests

from django.conf import settings

from .models import Photo


class TokenNotFoundException(Exception):
    pass


class PhotosDownloader:
    auth_api_key = settings.AUTH_API_KEY
    base_url = settings.PHOTOS_BASE_URL

    def _get_token(self):
        url = '{}/auth'.format(self.base_url)
        response = requests.post(url, json={'apiKey': self.auth_api_key})
        if response.status_code == 200:
            self.token = response.json().get('token')
        else:
            raise TokenNotFoundException

    def get_photos_list(self):
        headers = self._get_headers()
        has_more = True
        page = 1
        while has_more:
            objects_to_save = []
            url = '{}/images/?page={}'.format(self.base_url, page)
            response = requests.get(url, headers=headers)
            data = response.json()
            for picture_dict in data.get('pictures', []):
                objects_to_save.append(
                    Photo(external_id=picture_dict.get('id'), cropped_picture=picture_dict.get('cropped_picture'))
                )
            Photo.objects.bulk_create(objects_to_save, ignore_conflicts=True)
            has_more = data.get('hasMore')
            page += 1

    def _get_headers(self):
        if not hasattr(self, 'token'):
            self._get_token()
        return {'Authorization': 'Bearer {}'.format(self.token)}

    def get_photo(self, photo_id):
        headers = self._get_headers()
        photo_obj = Photo.objects.filter(id=photo_id).first()
        if not photo_obj:
            return
        url = '{}/images/{}/'.format(self.base_url, photo_obj.external_id)
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            data = response.json()
            photo_obj.author = data.get('author')
            photo_obj.camera = data.get('camera')
            photo_obj.tags = data.get('tags')
            photo_obj.cropped_picture = data.get('cropped_picture')
            photo_obj.full_picture = data.get('full_picture')
            photo_obj.save()
        return





