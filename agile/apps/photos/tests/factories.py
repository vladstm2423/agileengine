import factory

from photos.models import Photo


class PhotoFactory(factory.DjangoModelFactory):
    external_id = '1a5e86953ad5ac438130'
    author = 'Cool Respond'
    camera = 'Pentax 645D'
    tags = '#wonderfulday #wonderfullife #greatview #photooftheday #life #whataview '
    cropped_picture = 'http://interview.agileengine.com/pictures/cropped/0002.jpg'
    full_picture = 'http://interview.agileengine.com/pictures/full_size/0002.jpg'

    class Meta:
        model = Photo
