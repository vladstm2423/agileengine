from django.db import models
from django.utils.translation import gettext_lazy as _


class Photo(models.Model):
    external_id = models.CharField(_('External ID'), max_length=100, unique=True)
    author = models.CharField(_('Author'), max_length=50, blank=True, null=True)
    camera = models.CharField(_('Camera'), max_length=50, blank=True, null=True)
    cropped_picture = models.URLField(_('Cropped picture URL'), blank=True, null=True)
    full_picture = models.URLField(_('Full picture URL'), blank=True, null=True)
    tags = models.TextField(_('Tags'), blank=True, null=True)
    date_created = models.DateTimeField(_('Date created'), auto_now_add=True)
    date_updated = models.DateTimeField(_('Date created'), auto_now=True)

    class Meta:
        ordering = ('-date_created',)
        verbose_name = _('Photo')
        verbose_name_plural = _('Photos')
