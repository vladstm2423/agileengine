from rest_framework import serializers

from .models import Photo


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('external_id', 'author', 'camera', 'cropped_picture', 'full_picture', 'tags')
